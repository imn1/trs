use chrono::{Datelike, Local, NaiveTime, Weekday};
use std::sync::Arc;

use crate::config::Config;
use crate::config::Stop;
use crate::db::Record;

#[derive(Debug)]
pub struct Departure<'a> {
    pub stop: &'a Stop,
    pub departures: Vec<Record>,
}

impl Departure<'_> {
    pub fn get_departures_in_regard_delays(&self) -> Vec<&Record> {
        let now = Local::now().time();

        self.departures
            .iter()
            .filter(|d| {
                let mut stop_time =
                    NaiveTime::from_num_seconds_from_midnight_opt(d.stop_time.unwrap(), 0)
                        .expect("Wrong timestamp");

                // Consider delays.
                if let Some(add) = d.additionals {
                    if let Some(del) = add.delay {
                        stop_time += del;
                    }
                }

                stop_time > now
            })
            .collect()
    }

    #[cfg(feature = "prague")]
    pub async fn spice_up_additionals(&mut self) {
        use crate::features::prague;

        prague::spice_up_departures(&mut self.departures).await;
    }

    #[cfg(not(feature = "prague"))]
    pub async fn spice_up_additionals(&mut self) {}
}

pub struct Timetables {
    config: Arc<Config>,
}

impl<'a> Timetables {
    pub async fn new(config: Arc<Config>) -> Self {
        Timetables { config }
    }

    pub fn get_departures(&self, limit: usize) -> Vec<Departure> {
        let mut departures = vec![];

        for stop in self.config.stops.iter() {
            let mut next_deps = self.get_next_departures(stop, limit);
            let mut prev_deps = self.get_previous_departures(stop, limit);

            prev_deps.append(&mut next_deps);

            departures.push(Departure {
                stop,
                departures: prev_deps,
            });
        }

        departures
    }

    fn get_todays_departures(&self, stop: &'a Stop) -> impl Iterator<Item = &'a Record> {
        let now = Local::now();
        let date = now.naive_utc().date();

        stop.database
            .records
            .iter()
            // Filter for date.
            .filter(move |r| {
                if r.calendar.start_date <= date && r.calendar.end_date >= date {
                    return true;
                }

                false
            })
            // Filter for week day.
            .filter(move |r| match now.weekday() {
                Weekday::Mon => r.calendar.monday,
                Weekday::Tue => r.calendar.tuesday,
                Weekday::Wed => r.calendar.wednesday,
                Weekday::Thu => r.calendar.thursday,
                Weekday::Fri => r.calendar.friday,
                Weekday::Sat => r.calendar.saturday,
                Weekday::Sun => r.calendar.sunday,
            })
            // Only those lines with stop time (those who actually stop on the stop).
            .filter(|r| r.stop_time.is_some())
    }

    // TODO: async
    fn get_next_departures(&self, stop: &'a Stop, limit: usize) -> Vec<Record> {
        let now = Local::now();

        let mut filtered_and_sorted = self
            .get_todays_departures(stop)
            .filter(|r| {
                let timestamp = r.stop_time.unwrap();

                // Arrives "tomorrow"
                if timestamp > 60 * 60 * 24 {
                    // timestamp -= 60 * 60 * 24;
                    return true;
                }

                NaiveTime::from_num_seconds_from_midnight_opt(timestamp, 0)
                    .expect("Wrong timestamp.")
                    .ge(&now.time())
            })
            .cloned()
            .collect::<Vec<Record>>();

        // Sort by stop time (arrival time).
        filtered_and_sorted.sort_by_key(|r| r.stop_time);
        filtered_and_sorted.truncate(limit);

        filtered_and_sorted
    }

    fn get_previous_departures(&self, stop: &'a Stop, limit: usize) -> Vec<Record> {
        let now = Local::now();

        let mut filtered_and_sorted = self
            .get_todays_departures(stop)
            .filter(|r| {
                let timestamp = r.stop_time.unwrap();

                if timestamp > 60 * 60 * 24 {
                    return false;
                }

                NaiveTime::from_num_seconds_from_midnight_opt(timestamp, 0)
                    .expect("Wrong timestamp.")
                    .le(&now.time())
            })
            .cloned()
            .collect::<Vec<Record>>();

        // Sort by stop time (arrival time).
        filtered_and_sorted.sort_by_key(|r| r.stop_time);
        filtered_and_sorted.reverse();
        filtered_and_sorted.truncate(limit);
        filtered_and_sorted.reverse();

        filtered_and_sorted
    }
}
